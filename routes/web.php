<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Illuminate\Routing\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/users', function(){
    return App\User::get();
});

Route::get('/users/{user}', function($user){
    return App\User::find($user);
});

Route::get('/users/{name}/{email}', function($name,$email){
    $users = App\User::where('name',$name)
                        ->where('email',$email)
                        ->get();

    return $users;
});

//http://oraclembpj.test/pengguna
Route::get('pengguna', 'UserController@index');
Route::get('reset_token/{user}', 'UserController@reset_token');

Route::get('pengguna/{user}/{email?}', 'UserController@show');

Route::get('oracle1/{number}',function($no1){
    $pdo = DB::getPdo();
    $p1 = $no1;

    $smtp = $pdo->prepare("begin myproc(:p1,:p2); end;");
    $smtp->bindParam(':p1',$p1, PDO::PARAM_INT);
    $smtp->bindParam(':p2',$p2, PDO::PARAM_INT);
    $smtp->execute();

    return $p2;
});

Route::get('oracle2',function(){
    return DB::transaction(function ($conn) {
        $pdo = $conn->getPdo();
        $query = "a";
        $sql = "begin get_user_by_alpha(:user_alpha,:usr_result); end;";
        $stmt = $pdo->prepare($sql);

        $stmt->bindParam(':user_alpha', $query, PDO::PARAM_STR);
        $stmt->bindParam(':usr_result', $result, PDO::PARAM_STMT);

        $stmt->execute();

        oci_execute($result,OCI_DEFAULT);
        oci_fetch_all($result, $array, 0, -1, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC);
        oci_free_cursor($result);

        return $array;
    });
});


Route::get('HR/index', 'HRController@index');
