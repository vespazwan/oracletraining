<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    function manager() {
        return $this->belongsTo('App\User','user_id');
    }
}
