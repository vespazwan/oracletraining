<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personal extends Model
{
    protected $table = 'SPP.HR_V_PERSONAL';
    protected $primaryKey = 'HR_NO_PEKERJA';

    public function tanggungan(){
        return $this->hasOne('App\Tanggungan','HR_NO_PEKERJA');
    }
}
