<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;

class AuthController extends Controller
{
    public function login(Request $request){
        $request->validate([
            'username'=>'required|string',
            'password'=>'required|string'
        ]);

        $credentials = request(['username','password']);

         $user = \App\User::where([
             'USERNAME' => $request->username,
             'USERPASSWORD' => md5($request->password)
         ])->first();

        Auth::login($user);

        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');

        $token = $tokenResult->token;

        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);

        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }
}
