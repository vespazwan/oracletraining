<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Personal;
use App\Http\Resources\Personal as PersonalResource;
use DB;
use Illuminate\Support\Facades\View;


class APIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $personal = Personal::paginate(30);

        // return PersonalResource::collection($personal);

        $personal = DB::table('SPP.HR_V_PERSONAL as a')
        ->select('a.*','c.*','b.*')
        ->join('SPP.HR_MAKLUMAT_TANGGUNGAN as b ','a.HR_NO_PEKERJA','=','b.HR_NO_PEKERJA')
        ->join('SPP.HR_JAWATAN as c','a.HR_KOD_JAWATAN','=','c.HR_KOD_JAWATAN')
        ->where('a.HR_NO_PEKERJA','01049')
        ->get();

       // dd($personal);
        return View::make('personal.index')->with('personal',$personal);
        //return response()->json(['data'=>$personal]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
