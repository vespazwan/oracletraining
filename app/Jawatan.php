<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawatan extends Model
{
    protected $table = 'SPP.HR_JAWATAN';
    protected $primaryKey = 'HR_KOD_JAWATAN';
}
