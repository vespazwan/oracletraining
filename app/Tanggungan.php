<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tanggungan extends Model
{
    protected $table = 'SPP.HR_MAKLUMAT_TANGGUNGAN';
    protected $primaryKey = 'HR_NO_PEKERJA';
}
